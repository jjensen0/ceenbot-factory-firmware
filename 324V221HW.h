/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

// ============================== includes ================================ //
//#include <limits.h>
//#include <ctype.h>
//#include <stdarg.h>
#include <stdio.h>
//#include <avr/io.h>
//#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#define  F_CPU 15000000L     				// required by delay.h to set proper timing (remaining
											//   CPU MIPS are used by interrupt)
#include <util/delay.h>
#include "5x7font.h"						// font required for graphic LCD
#include "stdlib.h"							// required for rand() function

// ============================== defines ================================= //

// Firmware revision: "1.103"
#define FIRM_REV_MAJ    1
#define FIRM_REV_MIN    104

// Firmware status
// Must be:
//  'a' - Alpha
//  'b' - Beta
//  'T' - Test Build
//  'R' - Release
#define FIRM_REV_STAT   'R'


#define MAGIC_NUMBER 42

// I/O related
#define set_A0  PORTB |= (1<<PB3)      		// -->A0 = 1
#define clear_A0 PORTB &= ~(1<<PB3)    		// -->A0 = 0
#define Red_LED_On PORTD |= (1<<PD5)		// turn on red LED
#define Red_LED_Off PORTD &= ~(1<<PD5)		// turn off red LED
#define Red_LED_Toggle PORTD ^= (1<<PD5)	// toggle red LED
#define Green_LED_On PORTD |= (1<<PD6)		// turn on green LED
#define Green_LED_Off PORTD &= ~(1<<PD6)	// turn off green LED
#define Green_LED_Toggle PORTD ^= (1<<PD6)	// toggle green LED
#define Speaker_Toggle PORTD ^= (1<<PD7)	// toggle speaker output

// DDS related
#define DDS_DIVISOR			8000			// 1/2 of the timer 0 interrupt frequency; used to
											//   determine when a DDS accumulator should overflow
											//   and flag a DDS event
#define DDS_BRAKE_FREQ		200				// Frequency that PWM is applied in brake mode
#define DISP_FREQ			2				// Frequency that LCD display should be updated

// Motor related
#define MOTOR_ZERO_POSITION 0x80000000		// "zero" position for motors

// BUMP BOT related
#define BUMP_TIME			2750			// Used to time how long to back the bot and turn the bot
#define BUMP_FORWARD		0				// Display mode for bump bot
#define BUMP_RIGHT			1				// Display mode for bump bot
#define BUMP_LEFT			2				// Display mode for bump bot
#define BUMP_BACKUP			3				// Display mode for bump bot

// Menu related
#define MODE_MAX			0x02			// the maximum number of modes
#define MENU_MODE			0x00			// Brings up the menu screen
#define BUMP_MODE			0x01			// runs the bot in bump bot mode
#define TANK_MODE			0x02			// uses PSX controller to run the bot in tank mode
#define CAR_MODE			0x03			// uses PSX controller to run bot in car mode
#define SINGLE_MODE			0x04			// Uses a single left stick to control bot
#define DEAD_MODE			0x05			// Lets the User select motor, speed, time of travel

#define DISPLAY_MAX			0x08			// The maximum number of display options
#define BATT_DISP			0x00			// Displays battery information
#define INST_DISP			0x01			// Displays instructions for the current mode
#define MODE_DISP			0x02			// Displays Mode it is in
#define CTRL_DISP			0x03			// Displays PSX controller information
#define SERVO_DISP			0x04			// Displays servo status
#define WHEEL_DISP			0x05			// Displays wheel revolutions
#define SPEED_DISP			0x06			// Displays analog/digital speed info
#define FIRMWARE_DISP		0x07			// Displays firmware levels
#define BATT_DIAGS_DISP	 	0x08			// Displays battery diagnostics


#define BUMPPAUSE_DISP		0xAA			// A screen only shown when bump mode is paused
#define BATT_DIAGS_INFO		0xAB			// A screen for choosing battery info
#define BATT_DIAGS_VHIST 	0xAC
#define BATT_DIAGS_CHIST	0xAD

//uiRC_ServoToggle

#define SERVO_MAIN_DISP		0x00
#define SERVO_CONFIG_DISP	0x01

#define SERVO_CONFIG_LEFT	0x02
#define SERVO_CONFIG_RIGHT	0x03
#define SERVO_CONFIG_START  0x04

// ATtiny48 green status LED functions
#define NORMAL_LED			0x00			// Green LED functions as normal (communication status)
#define PULSE_SLOW			0x01			// Green LED functions as "battery charged" indicator
#define PULSE_FAST			0x02			// Green LED functions as "battery charging" indicator

// LCD related
#define LCD_COLUMN_SET_H    0X10
#define LCD_COLUMN_SET_L    0X00
#define LCD_PAGE_SET        0xB0

#define LCD_X_MAX 128						// setting for a 128 pixel wide LCD, can be changed
											//   for larger or smaller screens
#define LCD_Y_MAX 4							// setting for a 4 byte (32 pixel) high LCD, can be changed
											//   for larger or smaller screens
#define BATT_0BAR			0x80			// battery 0%
#define BATT_1BAR			0x81			// battery 17%
#define BATT_2BAR			0x82			// battery 33%
#define BATT_3BAR			0x83			// battery 50%
#define BATT_4BAR			0x84			// battery 67%
#define BATT_5BAR			0x85			// battery 83%
#define BATT_6BAR			0x86			// battery 100%
#define BATT_CHG	 		0x87			// battery charging (up arrow)
#define BATT_AC 			0x88			// AC powered (small AC)
#define BATT_NEEDCHG		0x89			// battery needs charging (double !!)

#define UP_ARROW			0x8A
#define DOWN_ARROW			0x8B
#define RIGHT_ARROW			0x8C
#define LEFT_ARROW			0x8D
#define CIRCLE				0x8E
#define TRIANGLE			0x8F

// Battery related
#define RESET				0x00
#define BATT_WARNING		0x01
#define BATT_SHUTDOWN		0x02
#define CHARGING		 	0x03
#define CHARGED				0x04
#define AC_POWERED			0x05

#define BATT6V0				12738			// correlates to 6.0V: 0.000471V/ADC LSB
#define BATT6V4				13588			// correlates to 6.4V: 0.000471V/ADC LSB
#define BATT6V8				14437			// correlates to 6.8V: 0.000471V/ADC LSB
#define BATT7V2				15287			// correlates to 7.2V: 0.000471V/ADC LSB
#define BATT7V6				16136			// correlates to 7.6V: 0.000471V/ADC LSB
#define BATT8V0				16985			// correlates to 8.0V: 0.000471V/ADC LSB
#define BATT10V0			21231			// correlates to 10.0V: 0.000471V/ADC LSB
#define BATT10V8			22930			// correlates to 10.8V: 0.000471V/ADC LSB
#define BATT11V0			23355			// correlates to 11.0V: 0.000471V/ADC LSB

#define BATT6V0LONG			26092723		// correlates to 6.0V: 0.223uV/ADC LSB (for added 16-bit avg)

#define CHG6V0				14286			// correlates to 6.0V: 0.00042V/ADC LSB

#define BATT_HIST_LENGTH	30				// # battery charge records to store 
#define MAH_HIST_LENGTH		10				// # of mah hours charged/discharged records
#define MAH_HIST_KEY		0xFFFF			// key marking current position in charge history log

#define BATT_CHG_DROOP		0x2ce14			// correlates to 0.5% of fully charged 8.5V NiMh battery
											//   This number comes from a fully charged battery (8.5V):
											//   10-bit ADC val = 1024 * ( 8.5V * ( 475k / 1475k )) / 5Vref
											//   = 561d = 0x231h
											//   0.5% of this number is 2.805d.  Scaling by 2^5 samples in
											//   uiADC_History[ 1 ] and another 2^11 in ulADC_avg_reg
											//   multiplies the 2.805d * 2^16 = 183828d = 0x2ce14h
											//
											// The value would be 0x59C28h for 1% droop


//#define MAX_PWM_VAL		0x0226			// roughly correlates to 2A charge w/3A charger when fully charged (9.1V)
//#define START_PWM_VAL		0x0120			// 
//#define MIN_PWM_VAL		0x0080			// roughly correlates to xxmA charge w/3A charger
#define MAX_PWM_VAL			0x0420			// roughly correlates to 2A charge w/3A charger when fully charged (9.1V)
#define START_PWM_VAL		0x00be			// should start at ~250mA with a 12V charger and adjust from there
#define MIN_PWM_VAL			0x00be			// roughly correlates to xxmA charge w/3A charger

#define MAX_BATT_CHG		4800			// 4800 = 4800mAH
#define MAX_FAST_CHG_TIME	270				// 270min = 4.5 hrs > 1A charge rate
#define MAX_SLOW_CHG_TIME	540				// 540min = 9 hrs < 1A charge rate
#define FAST_CHG_CHK_TIME	4				// number of minutes between battery voltage samples during charge for > 1A charge
#define SLOW_CHG_CHK_TIME	15				// number of minutes between battery voltage samples during charge for < 1A charge
#define BATT_WARN_TIME		1				// 1 min
#define MIN_CHG_CURRENT 	0x3f20			// min charge current ~ 50mA idle charge
#define RISE_CHG_CURRENT	0x43ee			// approx 350mA (climbs quickly to this number then slower to MAX_CHG_CURRENT)
#define FAST_CHG_CURRENT	0x4f81			// approx 1A: used to determine if charger is "high capacity" > 1A
											// Was 4f81 (1000mA) in ver 1.004R //**4f13
#define MAX_CHG_CURRENT		0x536c			// max charge current values for uiADC_History[0]
											// approx 500mA
											// 0x4f13=-0.997A, 0x4f39=-1.005a, 0x4f8d=-1.021A,
											// 0x51a0=-1.144A, 0x536c=-1.25a, 0x57bc=-1.5A,
											// 0x5c0c=-1.75A,  0x605c = -2.00a
											// Was 0x536c (1.25A) in ver 1.004R //**46b5
#define ZERO_CURRENT		0x3de9			// if less than ZERO_CURRENT then battery not present
											// 0x3e66=-0.010a, 0x3e3b=0.001a, 0x3e0f=0.011a,
											// 0x3de9=0.020a, 0x3d20=0.067a


// PSX remote related
#define DIGITAL_MODE_BYTE   0x41	        // Byte indicator for 'Digital-Mode' data.
#define ANALOG_MODE_BYTE    0x73        	// Byte indicator for 'Analog-Mode' data. (Red Mode).
#define DIGITAL_TURN_SPEED  0x73            // Set a digital speed for bump bot and digital PSX mode
#define MIN_ANALOG_SPEED    0x0a			// Minimum analog bot speed
#define MAX_ANALOG_SPEED    0xc8			// Maximum analog bot speed
#define MIN_DIGITAL_SPEED   0x0a			// Minimum digital bot speed
#define MAX_DIGITAL_SPEED   0x96			// Maximum digital bot speed
#define ANALOG_SPEED        0x73            // Set an analog speed for tank mode
#define PSX_LOW_THRESHOLD	0x71			// Define dead band for PSX analog stick
#define PSX_HIGH_THRESHOLD	0x8e			// Define dead band for PSX analog stick
#define PSX_VALIDATION_NUM  0x02			// Number of successive PSX packets that have to be identical
											//   for a valid response
#define PSX_FW_THRESHOLD	0x0F			// High end tolerance for faulty PSX controllers
#define PSX_REV_THRESHOLD	0xF0			// Low end tolerance ''	''	''

// Controller bit definitions. (5th byte response 'response[ 4 ]').
#define L2_BIT              0x01
#define R2_BIT              0x02
#define L1_BIT              0x04
#define R1_BIT              0x08
#define TRI_BIT             0x10
#define CIR_BIT             0x20
#define X_BIT               0x40
#define SQR_BIT             0x80

// Other button bit definitions. (4th byte response 'response[ 3 ]').
// Except for the 'ANALOG ONLY 'mode, these bits are present in both digital
// mode and analog mode (Red Mode).
#define SLCT_BIT            0x01
#define JOYR_BIT            0x02        	// Analog mode only
#define JOYL_BIT            0x04        	// Analog mode only
#define STRT_BIT            0x08
#define DPUP_BIT            0x10
#define DPRGT_BIT           0x20
#define DPDWN_BIT           0x40
#define DPLFT_BIT           0x80

// ========================== function prototypes ========================= //
void ATmega324_init(void);
void SPI_Transmit( unsigned char, unsigned char ); // transmit a byte to addr (master mode) (addr, data)
unsigned char SPI_Receive( unsigned char, unsigned char ); // receive a byte from addr, dummy transmit data (master mode)
void Set_Slave_Addr(unsigned char);				// set 74HC138 slave select address lines
void Spkr_chirp( void );						// make a chirp, chirp sound from speaker
void PWMcharger(unsigned int);					// 
void (*funcptr)( void ) = 0x0000; 				// Set up function pointer to RESET vector.
unsigned int Calc_Analog_Speed( unsigned char ); // calculate bot analog speed

// Bump Bot related
void Bump_Bot ( void );							// Bump bot master routine
void Turn_Bot( unsigned char );					// Turns the bot for BUMP_TIME ms
void Back_Bot( void );							// Backs the bot up for BUMP_TIME ms

// LCD related
void LCD_init(void);
void LCD_write_cmd(unsigned char cmd);			// primitive for writing commands to LCD
void LCD_write_data(unsigned char dat);			// primitive for writing data to LCD
void LCD_SetCursor( unsigned char ucPage, unsigned char ucColumn ); // Sets the LCD page and column cursor position

void LCD_BL(unsigned char); 					// set LCD backlight intensity
static int LCD_putchar(char c, FILE *stream);	// Write a character directly to the LCD without using
												//   the 324 LCD buffer
void LCD_putcharXY(unsigned char, unsigned char, unsigned char); // Write a character at X,Y directly to the LCD without using
												//   the 324 LCD buffer
static FILE mystdout = FDEV_SETUP_STREAM(LCD_putchar, NULL, _FDEV_SETUP_WRITE); // to support printf stream redirection
void LCD_clear( void );							// calls LCDbuf_clear and sends to LCD
void LCD_splash( void );						// display splash intro
void Display_data( void );                  	// display data to the screen
void DisplayBatteryGraph ( void );				// display battery diagnostics in chart form

// CPU clock related
void clk_rate ( char );							// set CPU clock rate divisor (freq MHz)

// PSX controller related
void ProcessTankMode( void );					// if TANK_MODE is selected, perform tank mode functions
void ProcessCarMode( void );					// if CAR_MODE
void Read_PSX( void );							// read PSX controller and return data in response[] global array
void Set_PSX_Analog( void );					// force PSX into anaog mode

// ATtiny slave I/O related
void Process_Sensor_Data( void );				// Process ATtiny switch presses
unsigned char Read_Sensors( void );				// read sensor data from ATtiny
unsigned int Read_Tiny_Rev( void );				// read revision level from ATtiny
void GetTinyLongRev( volatile unsigned char *major, // Get 'full' revision info from 'Tiny.
                     volatile unsigned char *minor,
                     volatile char *status );
void RC_Servo( void ); 							// send servo data
void Tiny_LED_Status( unsigned char ucLED_Status ); // set green status LED function

void EEMEM_init( void );						//TODO


// ================================ globals =============================== //
// LCD related
volatile char current_column = 0;				// keeps track of the current LCD column address
volatile char current_page = 3;					// keeps track of the current LCD row (page 3=top, 0=bottom)

// Menu related
volatile unsigned char ucOpMode = 0;			// keeps track of the mode that the bot is in
												//    Mode 0: menu select
												//    Mode 1: Battery Stats
												//    Mode 2: Bump Bot
												//    Mode 3: PSX controlled
												//    Mode 4: Sleep Mode
volatile unsigned char ucDisplayMode = 2;		// keeps track of the display the bot is in
volatile unsigned char ucDisplayPrev = 2;		// keeps track of the display the bot is in
volatile unsigned char ucBumpMode;				// 0=forward, 1=turn, 2=back up
volatile unsigned char ucBumpPause = 0;			// 0=paused, 1=running
volatile unsigned char ucBumpTimeout = 0;		// 0=paused, 1=running

// Interrupt related
volatile unsigned char ucInt2Phase;				// timer 2 interrupt phase counter 0..7

// DDS Related
volatile unsigned int DDS_Motor_L_Accum;		// DDS accumulator for Motor L
volatile unsigned int DDS_Motor_R_Accum;		// DDS accumulator for Motor R
volatile unsigned int DDS_Brake_Accum;			// DDS brake accumulator
volatile unsigned int DDS_L_Pulse_Timer;		// DDS count down timer for Motor L;
												//   when 0, turn off motor to save power
volatile unsigned int DDS_R_Pulse_Timer;		// DDS count down timer for Motor R;
												//   when 0, turn off motor to save power
volatile unsigned int DDS_spkr_Accum = 0;		// DDS accumulator for speaker
volatile unsigned char DDS_scratch_reg;			// scratch pad register for DDS functions
volatile unsigned int DDS_display_Accum;		// DDS accumulator for display update frequency
volatile unsigned char Display_Now = 1;			// If Display_Now = 1 then we can update the LCD with
												//   data.  Set by DDS TMR1 interrupt, reset by 
												//   display routine.

// Speaker Related
volatile unsigned int Spkr_Freq = 0;	 		// speaker frequency (Hz)
volatile unsigned int uiSpkr_Timer = 0;			// length of time to allow speaker beep

// Stepper Motor-Related
// Stepper phase pattern LUT.  It holds the pattern used to control the H-bridges that
//   control the Bipolar Stepper Motor.
const char Motor_L_LUT[] = { 0b00010000,
						 	 0b00000100,
							 0b00001100,
							 0b00011000 };
const char Motor_R_LUT[] = { 0b11000000,
							 0b01100000,
						 	 0b00100000,
							 0b10000000 };
volatile int Motor_PWM_LUT[] = { 1700, 1600, 1500, 1400, 1300, 1200, 1100, 1000, 900,
							 800, 700, 600, 500 }; 

volatile unsigned char Motor_Accel_Rate;		// Motor acceleration rate								  
volatile unsigned char Motor_Decel_Rate;		// Motor deceleration rate								  
volatile unsigned char ucMotor_L_Speed;			// speed of Motor L in pulses / sec
volatile unsigned char ucMotor_R_Speed;			// speed of Motor R in pulses / sec
volatile unsigned int Motor_L_Accel_Reg;		// internal variable - not for general use
												//   current motor speed based on acceleration
volatile unsigned int Motor_R_Accel_Reg;		// internal variable - not for general use
												//   current motor speed based on acceleration
volatile unsigned long ulMotor_L_Pos;			// position Motor L is to travel to
volatile unsigned long ulMotor_R_Pos;			// position Motor R is to travel to
volatile unsigned long ulMotor_L_Pos_Reg;		// internal variable - not for general use
												//   current motor position
volatile unsigned long ulMotor_R_Pos_Reg;		// internal variable - not for general use
												//   current motor position
volatile unsigned char ucMotor_L_Dir;			// Motor L direction 1=CW, 0=CCW
volatile unsigned char ucMotor_R_Dir;			// Motor R direction 1=CW, 0=CCW
volatile unsigned char ucMotor_L_State;			// Motor L phase state (0x00-0x03)
volatile unsigned char ucMotor_R_State;			// Motor R phase state (0x00-0x03)
volatile unsigned long Motor_L_Revs;			// Motor L revolutions
volatile unsigned long Motor_R_Revs;			// Motor R revolutions
volatile unsigned char ucBrakeMode;				// 1=brakes, 0=no brakes

// Battery and Power related
volatile unsigned int	uiPWMval;				// charger PWM value seeking 1A charge
volatile unsigned char	ucADC_Read_State = 0;	// ADC Channel currently being sampled (0x00-0x02)
volatile unsigned int	uiADC_History[4];		// ADC accumulated history of channel 0-3
volatile unsigned int	uiADC_Instant[4];		// ADC instantaneous value for channel 0-3
volatile unsigned int	uiADC_avg_reg;
volatile unsigned long	lBatt_Avg;				// 2^11 * 2^5 oversampling of battery voltage
volatile unsigned long  lBatt_Peak;				// comparison value of 2^11 * 2^5 oversampling of battery voltage
volatile unsigned long	ulADC_avg_reg;
volatile unsigned int  	uiBattCtr;				// Counter/timer for displaying battery status and analyzing system power
volatile double 		dMaH_ctr;				// mAh counter
volatile unsigned char	ucChgSampleTime; 		// Number of minutes between battery voltage samples during charge to
												//   determine when end of charge occurs
volatile unsigned int	uiChgMaxTime;	 		// Maximum number of minutes to charge a battery
volatile unsigned char	ucChgSecCtr;			// Timeout second counter for charge system
volatile unsigned int	uiChgMinCtr;			// Timeout minute counter for charge system
volatile unsigned char	ucBattDispState = 0x80; // Battery charger disp state for battery bargraph
volatile unsigned char  ucBattChgState = RESET; // Battery charger state
volatile unsigned int   uiBattCurrent;			// instantaneous battery current (not averaged)
volatile unsigned long  ulBatt_diags[BATT_HIST_LENGTH];	// Battery voltage history over time (logged & updated every 15 minutes)
volatile unsigned char  ucBatt_diags_index = 0;	// Battery voltage history index
volatile unsigned char  ucBatt_disp_index = 0;	// Battery voltage history display index (which record to start displaying)
volatile unsigned char  ucMenuToggleMode = 0;	// Toggles between settings and menu mode (BATT_DIAGS_DISP)
volatile unsigned char  ucPowerStatus; 			// System power status (1=yes, 0=no)
												//    bit 7 = battery present
												//    bit 6 = battery power switch on
												//    bit 5 = charger present
												//    bit 4 = charging
												//    bits 3-0 not used


//volatile double			MaH_history;					// amount charged or discharged
//volatile unsigned char	charge_history;					// 1 = charged		0 = discharged


// SPI related
volatile unsigned char SPI_data;
//volatile unsigned int uiTinyRev;				// ATtiny firmware revision level
// Desc: These are used for storing revision information (long form)
//       obtained with from the 'Tiny.
volatile unsigned char TinyRev_maj = 0;			// Holds ATTiny firmware 'major' revision level.
volatile unsigned char TinyRev_min = 0;			// Holds ATTiny firmware 'minor' revision level.
volatile char          TinyRev_stat = '?';		// Holds ATTiny firmware 'status' revision level.

// PSX remote related
volatile unsigned char response[ 21 ];			// Data storage buffer for incoming PSX data
volatile unsigned char analog = 0;				// status byte: analog=1, digital=0
volatile unsigned int uiPSX_TimeoutCtr; 		// PSX inactivity timeout counter (start with non-zero count)
												//   0 = valid data
												//   <> 0 = invalid data
volatile unsigned char ucPSX4Change;			// PSX byte 4 status: if any bit is a "1", this bit has changed
volatile unsigned char ucPSX4History;			// PSX byte 4 history: value of PSX byte 4 from previous read
volatile unsigned char ucPSX4State;				// State machine register for PSX byte 4
volatile unsigned char ucPSX4Validated_Data; 	// History register for byte 4 state machine
volatile unsigned char ucPSX4State_History; 	// History register for byte 4 state machine
volatile unsigned char ucBotAnalogSpeed;		// max bot speed (analog)
volatile unsigned char ucBotDigitalSpeed;		// max bot speed (digital)
volatile unsigned char ucPSXL2R2State;			// PSX L2 & R2 state machine variable - prevents L2 & R2 speed
												//   selection buttons from changing speeds too quickly


// PSX Poll command (with padded data).
volatile const unsigned char poll[ 21 ] = {		// main poll cmd
	0x01,0x42,0x00,0xFF,0xFF,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00};
volatile const unsigned char poll3[ 5 ] = {		// Enter Config Mode
	0x01,0x43,0x00,0x01,0x00 };
volatile const unsigned char poll4[ 9 ] = {		// Exit Config Mode
	0x01,0x43,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00};
volatile const unsigned char poll2[ 9 ] = {		// Force Analog On
	0x01,0x44,0x00,0x01,0x03,
    0x00,0x00,0x00,0x00};
/*
volatile const unsigned char poll5[ 9 ] = {		// Turn Motors On  
	0x01,0x4D,0x00,0x00,0x01,               	// Haven't Figured this out yet
    0xFF,0xFF,0xFF,0xFF};
*/

// ATtiny slave I/O related
volatile unsigned char ucSensorStatus;			// switch and IR bump sensor data
volatile unsigned long uiRC_ServoHist[ 5 ];		// RC servo history to decide if we need to send RC servo data
												//   to ATtiny.  Sending messages to the ATtiny result in RC servo
												//   timing deviations so minimizing messages helps to stabilize 
												//   servo timing.
volatile unsigned long uiRC_ServoData[ 5 ] = { 1450, 1450, 1450, 1450, 1450 };		// global data used by RC_Servo routine to position RC Servos

volatile unsigned int uiRC_ServoToggle = 0;
volatile unsigned int uiRC_ServoIndex = 0;
volatile unsigned int uiRC_ServoIndex2 = 1;
volatile unsigned int uiRC_ServoConfig = 0;

volatile unsigned int RCS_speed[ 5 ] = { 75, 75, 75, 75, 75 };
volatile unsigned int RCS_cmax[ 5 ] = { 2300, 2300, 2300, 2300, 2300 };
volatile unsigned int RCS_cmin[ 5 ] = { 600, 600, 600, 600, 600 };
volatile unsigned int RCS_absmax[ 5 ] = { 2300, 2300, 2300, 2300, 2300 };
volatile unsigned int RCS_absmin[ 5 ] = { 600, 600, 600, 600, 600 };

uint8_t EEMEM EEMEM_LOCK;
volatile unsigned long EEMEM RCS_max_EEMEM[5];
volatile unsigned long EEMEM RCS_min_EEMEM[5];
volatile unsigned long EEMEM uiRC_ServoData_EEMEM[5];

/*
//volatile double			EEMEM 	MaH_history_EEMEM[MAH_HIST_LENGTH];				// amount charged or discharged
//volatile unsigned char	EEMEM	charge_history_EEMEM[MAH_HIST_LENGTH];				// 1 = charged		0 = discharged

//volatile double			MaH_history[MAH_HIST_LENGTH];				// amount charged or discharged
//volatile unsigned char	charge_history[MAH_HIST_LENGTH];				// 1 = charged		0 = discharged

//volatile unsigned int	MaH_history_index;
//volatile unsigned int	MaH_history_display = 0;
*/

volatile unsigned char ucTest;
